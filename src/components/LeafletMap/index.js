import React, { useRef, useState } from 'react';
import { Map, TileLayer } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';

const LeafletMapComponent = ({center, zoom}) => {
    const [mapZoom, setMapZoom] = useState(15);
    const [position, setPosition] = useState(center)

    const mapRef = useRef(null);

    const handleLeafletMapClick = (e) => {
        console.log(e)
        const lat = e.latlng.lat;
        const lng = e.latlng.lng;
        setPosition({
          lat: lat,
          lng: lng
        })
        // alert(`New Location:\rlat: ${lat}\rlng: ${lng}`)
      }
      return(
        <Map 
        center={position} 
        zoom={mapZoom} 
        onclick={handleLeafletMapClick} 
        ref={mapRef} 
        onzoomend={() => setMapZoom(mapRef.current.leafletElement.getZoom())}
        >
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url='https://{s}.tile.osm.org/{z}/{x}/{y}.png'
        />
        <div style={{position: 'absolute', top: '50%', left: '50%', zIndex: 400}}>
            <img src={require("../../assets/pin.png")} alt="pin" id="location-pin"/>
        </div>
        </Map>
      )
}

export default LeafletMapComponent;