import React from 'react';

const LocationPin = ({ text }) => {

    return(
        <div className="pin">
            <img src={require("../assets/pin.png")} alt="pin" id="location-pin"/>
        </div>
    )
}

  export default LocationPin;