import React, {useState} from 'react'
import GoogleMap from 'google-map-react'
import LocationPin from '../LocationPin';

const GoogleMapComponent = ({center, zoom}) => {
    const [position, setPosition] = useState(center)

    const handleMapClick = ({x, y, lat, lng, event}) => {
        console.log(x, y, lat, lng, event)
        setPosition({
          lat: lat,
          lng: lng
        })
        // alert(`New Location:\rlat: ${lat}\rlng: ${lng}`)
      }
      
    return(
        <GoogleMap
          onClick={handleMapClick}
          // bootstrapURLKeys={{ key: "AIzaSyAmVCYCvKBXIQv6hv_fQbenimYtfSG6yc4", region: "fa", language: 'en' }}
          defaultCenter={position}
          defaultZoom={zoom}
          yesIWantToUseGoogleMapApiInternals
        >
          <LocationPin
            lat={position.lat}
            lng={position.lng}
            text="My Marker"
          />
        </GoogleMap>
    )
}

export default GoogleMapComponent;