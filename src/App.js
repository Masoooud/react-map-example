import React from 'react';
import './App.css';
import GoogleMapComponent from './components/GoogleMap';
import LeafletMapComponent from './components/LeafletMap';

const App = () => {

  const center={lat: 35.820645,lng: 51.491847}
  
  return (
    <div className="App">
      <div className="map">
        <GoogleMapComponent center={center} zoom={15}/>
      </div>
      <div className="map">
        <LeafletMapComponent center={center} zoom={15}/>
      </div>
    </div>
  );
}

export default App;
